#include "dbmanager.h"
#include <QSqlQuery>
#include <QDebug>
#include <QVariant>
DbManager::DbManager(QObject *parent) : QObject(parent)
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    #ifdef Q_OS_LINUX
    // NOTE: We have to store database file into user home folder in Linux
    QString path(QDir::home().path());
    path.append(QDir::separator()).append("my.db.sqlite");
    path = QDir::toNativeSeparators(path);
    db.setDatabaseName(path);
    #else
    // NOTE: File exists in the application private folder, in Symbian Qt implementation
    db.setDatabaseName("my.db.sqlite");
    #endif
    qDebug() << db.driverName() << db.hostName() << db.connectionName() << db.connectOptions() << db.databaseName();
}

DbManager::~DbManager()
{

}

bool DbManager::openDB()
{
    qDebug() << "opening db " << db.isOpen();
    if(db.isOpen())
        return true;

    // Open databasee
    return db.open();
}

QSqlError DbManager::lastError()
{
    // If opening database has failed user can ask
    // error description by QSqlError::text()
    return db.lastError();
}

void DbManager::closeDB()
{
    qDebug() << "closing db "<<db.isOpen();
    if(db.isOpen())
        db.close();
}

bool DbManager::checkAndCreateTables()
{
    bool ret=true;
    if (!db.isOpen())
        openDB();
    QSqlQuery query;

    if(!db.tables().contains( QLatin1String("key_val") ))
    {
        ret = query.exec(
                    "create table key_val "
                    "("
                    "id integer primary key,"
                    "key varchar(255)"
                    ");"
                    );
        qDebug() << query.lastQuery();
    }else
        qDebug() << "db already created";
    closeDB();
    return ret;
}

QString DbManager::getKey()
{
    QString key;
    checkAndCreateTables();
    openDB();
    QSqlQuery query;
    query.exec("SELECT id,key from key_val;");
    qDebug() << "SqlError: "<<lastError() << query.lastQuery();
    qDebug() << "result size: "<<query.size();
    while (query.next()) {
        key = query.value(1).toString();
        int id = query.value(0).toInt();
        qDebug() << id << key;
     }
    return key;
}

void DbManager::saveKeyInDb(QString key)
{
    checkAndCreateTables();
    openDB();
    QSqlQuery query;
    query.exec("DELETE FROM key_val;");
    qDebug() << "save SqlError: "<<lastError();
    query.exec("VACUUM;");
    qDebug() << "save SqlError: "<<lastError();
    QString query_to_exec = "INSERT INTO key_val(id,key) VALUES (NULL,\""+key+"\");";
    query.exec(query_to_exec);
    qDebug() << "save SqlError: "<<lastError() << query.lastQuery();;
    query.exec("SELECT id,key from key_val;");
    qDebug() << "SqlError: "<<lastError();
    qDebug() << "result size: "<<query.size() << query.lastQuery();
    closeDB();
}

