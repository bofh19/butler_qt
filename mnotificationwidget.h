#ifndef MNOTIFICATIONWIDGET_H
#define MNOTIFICATIONWIDGET_H

#include <QWidget>
#include <QPixmap>
#include <QPushButton>
namespace Ui {
class MNotificationWidget;
}

class MNotificationWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MNotificationWidget(QWidget *parent = 0);
    void setIcon(const QPixmap &icon);
    void setDesc(const QString& desc);
    void setDesc2(const QString& desc2);
    QPushButton *getButton();
    ~MNotificationWidget();

private slots:
    void on_pushButton_clicked();

private:
    Ui::MNotificationWidget *ui;
};

#endif // MNOTIFICATIONWIDGET_H
