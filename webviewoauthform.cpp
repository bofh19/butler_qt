#include "webviewoauthform.h"
#include "ui_webviewoauthform.h"
#include <QWebFrame>
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

WebViewOauthForm::WebViewOauthForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WebViewOauthForm)
{
    ui->setupUi(this);
    QUrl url("https://accounts.google.com/o/oauth2/auth?redirect_uri=http://rmrf.w4rlock.in/api/auth/2/&response_type=code&client_id=405284180877-sepfua7de171m21oguu77rkgh8u7qtai.apps.googleusercontent.com&scope=email&approval_prompt=force");
    ui->webView->setUrl(url);
}

WebViewOauthForm::~WebViewOauthForm()
{
    delete ui;
}

void WebViewOauthForm::on_webView_loadFinished(bool arg1)
{
    QString str = ui->webView->url().toString();
    qDebug() << "URL: "<<str;
    if (str.contains("http://rmrf.w4rlock.in/api/auth/2/?code=4"))
    {
        qDebug() << "CONTAINS";
        QJsonDocument doc = QJsonDocument::fromJson(ui->webView->page()->mainFrame()->toPlainText().toUtf8());
        QJsonObject obj = doc.object();
        QString api_key = QJsonValue(obj.value("api_key")).toString();
        qDebug() << api_key;
        emit foundApiKey(api_key);
        this->close();
    }
    else{
        qDebug() << "DOESNOTCONTAIN";
    }
}
