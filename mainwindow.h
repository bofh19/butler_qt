#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include "webviewoauthform.h"
#include "notificationmodel.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    QTcpSocket *_pSocket;
    WebViewOauthForm *wvo;
    QString api_key;
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QString getLengthStringFromLength(int length);
public slots:
    void readTcpData();
    void socketDisconnected();
    void socketConnected();
private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void tcp_heart_beat();
    void foundApiKey(QString api_key);
    void on_pushButton_3_clicked();

private:
    Ui::MainWindow *ui;
    QString mData;
    void process_fetched_data(QString fetched_data);
    NotificationModel *notificationModel;
};

#endif // MAINWINDOW_H
