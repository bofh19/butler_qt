#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTcpSocket>
#include <QTimer>
#include <QJsonDocument>
#include <QJsonObject>
#include "webviewoauthform.h"
#include "dbmanager.h"
#include <QJsonArray>
#include <QVariant>
#include <QTextCodec>
#include "mnotification.h"
#include "notificationmodel.h"
#include "notificationviewdelegate.h"
#include <QSystemTrayIcon>
#include <QMessageBox>
const QString S_R_DATA = "DATA";
const QString S_R_PONG = "PONG";
const QString S_R_END = "E_ND";
const QString S_R_ERROR = "ERRR";
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _pSocket = new QTcpSocket( this ); // <-- needs to be a member variable: QTcpSocket * _pSocket;
    connect( _pSocket, SIGNAL(readyRead()), SLOT(readTcpData()) );
    connect(_pSocket,SIGNAL(disconnected()),SLOT(socketDisconnected()));
    connect(_pSocket,SIGNAL(connected()),SLOT(socketConnected()));
    qDebug() << "api_key_len" << api_key.length();
    DbManager *db = new DbManager();
    api_key = db->getKey();
//    api_key = "7S27LJUQ3PRKSW4E";
    if(api_key.length() <= 0)
    {
        wvo = new WebViewOauthForm();
        connect(wvo,SIGNAL(foundApiKey(QString)),this,SLOT(foundApiKey(QString)));
        wvo->setWindowModality(Qt::ApplicationModal);
        wvo->show();
    }else{
        foundApiKey(api_key);
    }
    notificationModel = new NotificationModel(this);
    ui->listView->setModel(notificationModel);
    ui->listView->setItemDelegate(new NotificationViewDelegate(this));
    ui->listView->setStyleSheet( "QListWidget::item { border: 1px solid black; }" );
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::readTcpData()
{
    QString new_data = _pSocket->readAll();
    if(new_data.right(4).contains("E_ND"))
    {
        mData += new_data;
        qDebug() << mData;
        qDebug() <<"mData" <<mData.left(150) <<"......"<< mData.right(150);
        process_fetched_data(mData);
        mData = "";
    }else
    {
        mData += new_data;
    }

}

void MainWindow::socketDisconnected()
{
    qDebug() << "socket disconected trying to reconnect";
    _pSocket->connectToHost("rmrf.w4rlock.in", 6071);
    ui->statusBar->showMessage("disconnected...");
}

QString MainWindow::getLengthStringFromLength(int length)
{
    int max_length = 16;
    QString len = QString::number(length)+"";
    int no_of_zeros_missing = max_length - len.length();
    if (no_of_zeros_missing <= 0) return len;
    QString res = len;
    for(int i=0;i<no_of_zeros_missing;i++){
        res = "0"+res;
    }
    return res;
}

void MainWindow::socketConnected()
{
    qDebug() << "socket connected";
    QString *str = new QString("PING");
    QString *out_str = new QString(getLengthStringFromLength(str->length()) + api_key+ *str);
    qDebug() << "sending data: "<<out_str;
    QByteArray data = out_str->toLatin1();
    _pSocket->write(data);
    ui->statusBar->showMessage("Connected");
}

void MainWindow::on_pushButton_clicked()
{
    //QString str = ui->textEdit->toPlainText();
    //str = getLengthStringFromLength(str.length()) + api_key+str;
//    QByteArray data = str.toLatin1();
//    qDebug() << "sending data: "<<str;
//    //    _pSocket->connectToHost("rmrf.w4rlock.in", 9071);
//    if( _pSocket->waitForConnected() )
//    {
//        _pSocket->write( data );
//    }

}

void MainWindow::on_pushButton_2_clicked()
{
    if(_pSocket->isOpen())
    {
        qDebug() << "socket is open closing";
        _pSocket->close();
    }
}

void MainWindow::tcp_heart_beat()
{
    QString *str = new QString("PING");
    QString *out_str = new QString(getLengthStringFromLength(str->length()) + api_key+ *str);
    qDebug() << "sending data: "<<*out_str;
    QByteArray data = out_str->toLatin1();
    bool connected = (_pSocket->state() == QTcpSocket::ConnectedState);
    if(!connected)
    {
        qDebug() << "connection lost " << "trying to reconnect";
        _pSocket->connectToHost("rmrf.w4rlock.in", 6071);
    }
    if( _pSocket->waitForConnected() )
    {
        _pSocket->write( data );
    }
}

void MainWindow::foundApiKey(QString api_key)
{
    this->api_key = api_key;
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(tcp_heart_beat()));
    timer->start(60000); //time specified in ms every 60 secs
    _pSocket->connectToHost("rmrf.w4rlock.in", 6071);
    DbManager *db = new DbManager();
    db->saveKeyInDb(api_key);
}

void MainWindow::on_pushButton_3_clicked()
{

}

void MainWindow::process_fetched_data(QString fetched_data)
{
    qDebug() << "process_fetched_data started";
    if(fetched_data.left(4) == S_R_DATA)
    {
        MNotification *notif = new MNotification;
        QJsonDocument doc = QJsonDocument::fromJson(fetched_data.mid(4,fetched_data.length()-8).toUtf8());
        QJsonObject obj = doc.object();
        if(obj.contains("byteString"))
        {
            qDebug() << "contains byte string" ;//<< obj.value("byteString").isArray();//.toString();
            QByteArray barr = QByteArray::fromBase64(obj.value("byteString").toVariant().toByteArray());
            notif->base64array = barr;
        }

        if (obj.contains("package_name"))
            notif->package_name = obj.value("package_name").toString();

        if (obj.contains("notification_id"))
            notif->notification_id = obj.value("notification_id").toInt();

        if (obj.contains("EXTRA_BIG_TEXT"))
            notif->EXTRA_BIG_TEXT = obj.value("EXTRA_BIG_TEXT").toString();

        if (obj.contains("EXTRA_INFO_TEXT"))
            notif->EXTRA_INFO_TEXT = obj.value("EXTRA_INFO_TEXT").toString();

        if (obj.contains("EXTRA_SUB_TEXT"))
            notif->EXTRA_SUB_TEXT = obj.value("EXTRA_SUB_TEXT").toString();

        if (obj.contains("EXTRA_SUMMARY_TEXT"))
            notif->EXTRA_SUMMARY_TEXT = obj.value("EXTRA_SUMMARY_TEXT").toString();

        if (obj.contains("EXTRA_TEXT"))
            notif->EXTRA_TEXT = obj.value("EXTRA_TEXT").toString();

        if (obj.contains("EXTRA_TITLE"))
            notif->EXTRA_TITLE = obj.value("EXTRA_TITLE").toString();

        if (obj.contains("EXTRA_TITLE_BIG"))
            notif->EXTRA_TITLE_BIG = obj.value("EXTRA_TITLE_BIG").toString();

        if (obj.contains("status"))
            notif->status = obj.value("status").toString();
        notificationModel->addNewNotif(notif);
        QApplication::alert(this,1000);
//        QMessageBox::information(0, QString("Information"),
//                                 QString("You've pressed the button \"Press Me!\""),
//                                 QMessageBox::Ok);
    } else if(fetched_data.left(4) == S_R_PONG)
    {
        qDebug() << "just a pong ignore";
    } else if(fetched_data.left(4) == S_R_ERROR)
    {
        qDebug() << "error "<<fetched_data;
    } else
    {
        qDebug() << "what is this ?";
        qDebug() <<"data_fetching" <<fetched_data.left(10) <<"......"<< fetched_data.right(50);
    }

}
