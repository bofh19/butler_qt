#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QObject>
#include <QSqlError>
#include <QSqlDatabase>
class DbManager : public QObject
{
    Q_OBJECT
public:
    explicit DbManager(QObject *parent = 0);
    ~DbManager();
    bool openDB();
    QSqlError lastError();
    void closeDB();
    bool checkAndCreateTables();
    QString getKey();
    void saveKeyInDb(QString key);
private:
    QSqlDatabase db;
signals:

public slots:
};

#endif // DBMANAGER_H
