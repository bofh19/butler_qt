#ifndef NOTIFICATIONMODEL_H
#define NOTIFICATIONMODEL_H
#include <QAbstractListModel>
#include <QList>
#include <QtCore>
#include "mnotification.h"
class NotificationModel : public QAbstractListModel
{
    Q_OBJECT
public:
    NotificationModel(QObject* parent);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    void addNewNotif(MNotification* notif);
    QList<MNotification*> notifs;
    ~NotificationModel();

};

#endif // NOTIFICATIONMODEL_H
