#ifndef NOTIFICATIONVIEWDELEGATE_H
#define NOTIFICATIONVIEWDELEGATE_H
#include <QStyledItemDelegate>
#include <QPainter>
#include <QModelIndex>
#include <QStyleOptionViewItem>
#include <mnotificationwidget.h>
class NotificationViewDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    NotificationViewDelegate(QWidget *parent);
    ~NotificationViewDelegate();
    void paint(QPainter *painter, const QStyleOptionViewItem &option,const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option,const QModelIndex &index) const;
protected:
    bool editorEvent(QEvent *event, QAbstractItemModel *model,
                     const QStyleOptionViewItem &option, const QModelIndex &index);

private:
    MNotificationWidget *mNotificationView;
};

#endif // NOTIFICATIONVIEWDELEGATE_H
