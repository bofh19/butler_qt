#ifndef MNOTIFICATION_H
#define MNOTIFICATION_H
#include <QtCore>

class MNotification
{
public:
    MNotification();
    ~MNotification();
    QByteArray base64array;
    QString package_name;
    int notification_id;
    QString EXTRA_BIG_TEXT;
    QString EXTRA_INFO_TEXT;
    QString EXTRA_SUB_TEXT;
    QString EXTRA_SUMMARY_TEXT;
    QString EXTRA_TEXT;
    QString EXTRA_TITLE;
    QString EXTRA_TITLE_BIG;
    QString status;
    QString byteString;
};

#endif // NOTIFICATION_H
