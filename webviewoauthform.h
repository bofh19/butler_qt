#ifndef WEBVIEWOAUTHFORM_H
#define WEBVIEWOAUTHFORM_H

#include <QWidget>

namespace Ui {
class WebViewOauthForm;
}

class WebViewOauthForm : public QWidget
{
    Q_OBJECT

public:
    explicit WebViewOauthForm(QWidget *parent = 0);
    ~WebViewOauthForm();

private slots:
    void on_webView_loadFinished(bool arg1);

private:
    Ui::WebViewOauthForm *ui;

signals:
    void foundApiKey(QString api_key);
};

#endif // WEBVIEWOAUTHFORM_H
