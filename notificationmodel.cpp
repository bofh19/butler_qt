#include "notificationmodel.h"
#include <QDebug>

NotificationModel::NotificationModel(QObject *parent)
{

}

int NotificationModel::rowCount(const QModelIndex &parent) const
{
    return notifs.size();
}

QVariant NotificationModel::data(const QModelIndex &index, int role) const
{
    // Check that the index is valid and within the correct range first:
       if (!index.isValid()) return QVariant();
       if (index.row() >= notifs.size()) return QVariant();

       if (role == Qt::DisplayRole) {
           QString qstr = notifs.at(index.row())->EXTRA_TITLE;
           return QVariant(qstr);
       } else{
           return QVariant();
       }
}

void NotificationModel::addNewNotif(MNotification *notif)
{
    if(notif->package_name.length() <= 0)
    {
        return;
    }
    if(notifs.size()<=0 && notif->status == "ADDED")
    {
        beginResetModel();
        notifs.append(notif);
        endResetModel();
        return;
    }
    for(int whichNotif=0;whichNotif<notifs.length();++whichNotif)
    {
        MNotification* notification = notifs[whichNotif];
        if(notification->package_name == notif->package_name)
        {
            if(notification->status == notif->status)
            {
                // replacing with new one
                beginResetModel();
                notifs.replace(whichNotif,notif);
                endResetModel();
            }else{
                // if not the same then remove it
                beginResetModel();
                notifs.removeOne(notification);
                endResetModel();
            }
        }else{
            // its a diff notification
            if(notif->status == "ADDED"){
                qDebug() << "its a diff notification adding it "<<notif->status;
                beginResetModel();
                notifs.append(notif);
                endResetModel();
            }
        }

    }
}

NotificationModel::~NotificationModel()
{

}

