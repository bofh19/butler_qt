#-------------------------------------------------
#
# Project created by QtCreator 2015-01-12T13:01:12
#
#-------------------------------------------------

QT       += core gui network webkitwidgets sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Butler
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    webviewoauthform.cpp \
    dbmanager.cpp \
    notificationmodel.cpp \
    notificationviewdelegate.cpp \
    mnotification.cpp \
    mnotificationwidget.cpp

HEADERS  += mainwindow.h \
    webviewoauthform.h \
    dbmanager.h \
    notificationmodel.h \
    notificationviewdelegate.h \
    mnotification.h \
    mnotificationwidget.h
FORMS    += mainwindow.ui \
    webviewoauthform.ui \
    mnotificationwidget.ui
