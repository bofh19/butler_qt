#include "mnotificationwidget.h"
#include "ui_mnotificationwidget.h"
#include <QDebug>
MNotificationWidget::MNotificationWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MNotificationWidget)
{
    ui->setupUi(this);

}

void MNotificationWidget::setIcon(const QPixmap &icon)
{
    ui->icon->setPixmap(icon);
}

void MNotificationWidget::setDesc(const QString &desc)
{
    ui->desc->setText(desc);
}

void MNotificationWidget::setDesc2(const QString &desc2)
{
    ui->desc2->setText(desc2);
}

QPushButton *MNotificationWidget::getButton()
{
    return ui->pushButton;
}

MNotificationWidget::~MNotificationWidget()
{
    delete ui;
}

void MNotificationWidget::on_pushButton_clicked()
{
    qDebug() << "clicked on button" << ui->desc->text() << ui->desc2->text();
}
