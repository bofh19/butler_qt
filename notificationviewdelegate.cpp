#include "notificationviewdelegate.h"
#include "notificationmodel.h"
#include "mnotification.h"
#include "mnotificationwidget.h"
#include <QApplication>
#include <QStyleOptionButton>
#include <QDebug>
NotificationViewDelegate::NotificationViewDelegate(QWidget *parent)
{
    mNotificationView = new MNotificationWidget();
    mNotificationView->resize(parent->width()-10, mNotificationView->height());
}

NotificationViewDelegate::~NotificationViewDelegate()
{

}

void NotificationViewDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    const NotificationModel *notifModel = (NotificationModel*)index.model();
    const MNotification* notif = notifModel->notifs.at(index.row());

    QPixmap pixmap;
    QRect geometry = option.rect;
    int w = geometry.width(), h = geometry.height(), x = geometry.x(), y = geometry.y();
    pixmap.loadFromData(notif->base64array);
    pixmap = pixmap.scaled(80,80,Qt::KeepAspectRatio,Qt::FastTransformation);
//    painter->setPen(Qt::transparent);
//    painter->setBrush(QColor(0,0,0,155));
//    painter->drawRect(geometry);
//    painter->drawPixmap(x + 5, y + 5,80,80,pixmap);
//    painter->setPen(Qt::white);
//    painter->drawText(QRectF(x + 90 , y + 5, w - 120, h) , Qt::AlignLeft, notif->EXTRA_TITLE);
//    painter->drawText(QRectF(x + 90 , y + 22, w - 120, h) , Qt::AlignLeft, notif->EXTRA_TEXT);

    mNotificationView->setIcon(pixmap);
    mNotificationView->setDesc(notif->EXTRA_TITLE);
    mNotificationView->setDesc2(notif->EXTRA_TEXT + notif->EXTRA_INFO_TEXT + notif->EXTRA_BIG_TEXT+ notif->EXTRA_SUMMARY_TEXT);
    mNotificationView->render(painter, QPoint(0,10), QRegion(geometry), QWidget::DrawChildren);

//    QStyleOptionButton opt;
//    opt.state = QStyle::State_Enabled;
//    QPushButton *btn = mNotificationView->getButton();
//    QRect btnRect(btn->rect());
//    btnRect.setX( btnRect.x() + option.rect.x() );
//    btnRect.setY(btnRect.y() + option.rect.y() );
//    qDebug() << "btnRect" << btnRect.height()<<btnRect.width()<<btnRect.x()<<btnRect.y();
//    opt.rect = btn->rect();
//    opt.text = "ABCDEFGHIJKL";
//    QApplication::style()->drawControl(QStyle::CE_PushButton,&opt,painter);

}

QSize NotificationViewDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if(!index.isValid())
            return QSize();

    return QSize(100,100);;
}

bool NotificationViewDelegate::editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index)
{
    qDebug() << "an event fired from editor event";
}

